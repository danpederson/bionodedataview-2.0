from PyQt5 import QtGui, QtCore, QtWidgets
import pyqtgraph as pg
from TopLevelTabForm_UI import Ui_Form
import BionodeDataView_UI

import socket
import threading
import datetime
import configparser
import os.path
import BaseStation
import select
import time

#stimTimer = QtCore.QTimer()
#cdTimer = QtCore.QTimer()
UpdatePlotPeriod = 50


class BionodeDataView(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(BionodeDataView, self).__init__(parent)

        self.ui = BionodeDataView_UI.Ui_Form()
        self.ui.setupUi(self)

        self.ui.Version_l.setText('Version 17.10.0')

        self.numberOfChannelsDisplayed = 2
        self.SampleFrequencyStringMap = {'45 kHz': 45000, '40 kHz': 40000, '25 kHz': 25000, '22.5 kHz': 25500, '20 kHz': 20000, '16 kHz': 16000, '12.5 kHz': 12500, '10 kHz': 10000, '8 kHz': 8000, '6.25 kHz': 6250, '5 kHz': 5000, '2.5 kHz': 2500, '1.25 kHz': 1250, '500 Hz': 500, '250 Hz': 250}

        self.SampleFrequencyStrings_2ch = ['500 Hz', '2.5 kHz', '5 kHz', '12.5 kHz']
        self.SampleFrequencyStrings_4ch = ['250 Hz', '1.25 kHz', '2.5 kHz', '6.25 kHz']
        # These sample frequencies only available for Bionode Version > 4.1 with M4 MCU
        self.SampleFrequencyStrings_2ch_LargePacket = ['16 kHz', '20 kHz', '40 kHz', '45 kHz']
        self.SampleFrequencyStrings_4ch_LargePacket = ['8 kHz', '10 kHz', '20 kHz', '22.5 kHz']

        self.ADCCodes = {'Rec 1': 1, 'Rec 2': 2, 'Opt 1': 3, 'Opt 2': 4, 'Rs_H': 5, 'Rs_L': 6}

        #Labels for the graphs as defined by the ADCCodes index. Default is voltage, but this can be changed by updating the BionodeConfigSettings.ini file
        self.ADCLabels = ['','Voltage','Voltage','Voltage','Voltage','Voltage','Voltage']
        self.ADCUnits = ['','V','V','V','V','V','V']

        self.PAPowerRegisters = {'0.05': 0x00, '0.06': 0x01, '0.08': 0x02, '0.10': 0x03, '0.13': 0x04, '0.16': 0x05, '0.20': 0x06, '0.25': 0x07, '0.30': 0x08, '0.38': 0x09, '0.47': 0x0A, '0.59': 0x0B, '0.74': 0x0C, '0.93': 0x0D, '1.17': 0x0E, '1.45': 0x0F, '1.78': 0x10, '2.14': 0x11, '2.57': 0x12, '2.95': 0x13, '3.24': 0x14, '3.47': 0x15, '3.72': 0x16, '3.80': 0x17, '3.89': 0x18, '3.98': 0x19, '4.07': 0x1A, '4.17': 0x1B}

        self.closing = False
        self.stoppedSocket = True
        self.socketsOpened = False
        
        self.BionodeConfigSettingsPath = 'BionodeConfigSettings.ini'
        self.BaseStationConfigSettingsPath = 'BaseStationConfigSettings.ini'
        self.DataViewConfigSettingsPath = 'DataViewConfigSettings.ini'
        self.ADCrefVoltage = 1.8
        self.VoltsPerBit = self.ADCrefVoltage/256
        self.SampleRate = self.getSampleRateFromComboBox()/2
        self.displayTime = 2
        self.WindowSize = int(self.displayTime*self.SampleRate)
        self.dataToDisplay = []
        self.displayData = [-2 for i in range(self.WindowSize*2)]
        self.impedanceBuff = [0 for i in range(self.WindowSize*2)]

        self.prevTimeStamp = -1
        self.recording = False
        self.recordingFileSize = 0
        self.additionalRecordTime = 30 * 60 #sec
        self.autoStopStim = False
        self.stopStimTimer = 0

        self.newThermalDataReady = False
        self.thermalData = -1

        self.impedanceTestRunning = False

        self.ch1gain = 1000
        self.ch2gain = 1000
        self.ch3gain = 1000
        self.ch4gain = 1000
        self.ch1offset = 0.9
        self.ch2offset = 0.9
        self.ch3offset = 0.9
        self.ch4offset = 0.9

        self.stimCurrentLimitCnt=0
        self.currentLimitCountDown = 60 # during the 60sec period, if any over current event exceeds the limit set byt stimCurrentLimitCnt, system will stop stim.


        self.displayPaused = False

        self.SetUpDisplays()
        self.updatePlotVisibility(2)

        self.ui.ADC_3_l.setVisible(False)
        self.ui.ADC_4_l.setVisible(False)
        self.ui.ADC3Input_cb.setVisible(False)
        self.ui.ADC4Input_cb.setVisible(False)

        #hide the impedance test buttons and labels
        self.ui.StartImpedanceTest_b.setVisible(False)
        self.ui.StopImpedanceTest_b.setVisible(False)
        self.ui.ImpedanceTestChannel_l.setVisible(False)
        self.ui.ImpedanceTestCh_cb.setVisible(False)

        #setup stim buttons and labels
        self.ui.StartTimer_b.setEnabled(True)
        self.ui.StopTimer_b.setEnabled(False)
        self.ui.label_38.setVisible(False)
        self.ui.currentMeas_te.setVisible(False)
        self.ui.label_26.setVisible(False)
        self.ui.label_27.setVisible(False)

        self.stimTimer = QtCore.QTimer()
        self.cdTimer = QtCore.QTimer()


        #Connect UI Actions
        self.connectActions()

        #Set defauld textbox values
        self.ui.PulseAmplitude_te.setText('300')
        self.ui.PulseWidth_te.setText('500')
        self.ui.PulsePeriod_te.setText('2000')
        self.ui.InterphasicDelay_te.setText('100')
        self.ui.NumStimCycles_te.setText('100')
        self.ui.PositiveCalibration_te.setText('0')
        self.ui.NegativeCalibration_te.setText('0')
        self.ui.CathodicCalibration_te.setText('0')
        self.ui.AnodicCalibration_te.setText('0')
        self.ui.ZeroCalibration_te.setText('0')
        self.ui.Pot1Value_te.setText('128')
        self.ui.Pot2Value_te.setText('128')
        self.ui.ShundownThres_te.setText('2')
        self.ADCChannelRatio = self.ui.ADCSampleRatio_cb.currentText()
        self.ui.label_28.setText('x')
        self.ui.StimWaveform_cb.setCurrentIndex(0)
        self.ui.InterphasicDelay_te.setVisible(False)
        self.ui.InterphasicDelayUnits_l.setVisible(False)
        self.ui.InterphasicDelay_l.setVisible(False)
        self.ui.PulsePeriod_l.setText('Pulse Period')
        self.ui.displayTime_te.setText(str(self.displayTime))

        #Setup default positions of UI elements
        self.ui.ADCResolution_cb.setCurrentIndex(0)
        self.ui.ADC1Input_cb.setCurrentIndex(0)
        self.ui.ADC2Input_cb.setCurrentIndex(1)
        self.ui.SampleFrequency_cb.setCurrentIndex(1)
        self.ui.Enable4Channels_cb.setChecked(True)
        self.ui.ADCResolution_cb.setCurrentIndex(1)
        self.ui.BatteryOnOff_cb.setChecked(True)

        #self.ui.ChargeFrequency_te.setText('300')
        self.ui.ChargeFrequency_dsb.setValue(345.00)

        #If bionode config file exists, read it in and populate the Bionode combo box
        if (os.path.isfile(self.BionodeConfigSettingsPath)):
            self.bionodeConfig = configparser.ConfigParser()
            self.bionodeConfig.read(self.BionodeConfigSettingsPath)
            for bionode in self.bionodeConfig.sections():
                self.ui.Bionode_cb.addItem(self.ConfigSectionMap(self.bionodeConfig,bionode)['name'])

        # if (os.path.isfile(self.BaseStationConfigSettingsPath)):
        #     self.baseStationConfig = configparser.ConfigParser()
        #     self.baseStationConfig.read(self.BaseStationConfigSettingsPath)
        #     for baseStation in self.baseStationConfig.sections():
        #         self.ui.BaseStationAddress_cb.addItem(self.ConfigSectionMap(self.baseStationConfig,baseStation)['address'])

    def closeEvent(self, event):
        print('closing')

        try:
            self.closing = True
            startTime = time.time()
            
            if (self.socketsOpened):
                while not self.stoppedSocket:
                    if ((time.time() - startTime) > 5):
                        print('Timeout!')
                        break
                self.BaseStationController.CloseSockets()
            else:
                print('No Sockets open!')
        except:
            print('errors on closing...')
        
        if (self.recording):
            try:
                self.recordingFile.close()
            except:
                print('errors closing file')
        event.accept()

    def ConfigSectionMap(self,config,section):
        dict1 = {}
        options = config.options(section)
        for option in options:
            try:
                dict1[option] = config.get(section, option)
            except:
                dict1[option] = None
        return dict1

    def SetUpDisplays(self):
        self.plotTimer = QtCore.QTimer()
        self.plotTimer.timeout.connect(self.updatePlot)   

        #Set up the first plot
        self.pw1 = self.ui.graphicsView_1.getPlotItem()
        self.p1 = self.pw1.plot()
        self.p1.setPen((200,200,0))
        self.pw1.setXRange(0, self.WindowSize/(self.SampleRate))
        self.pw1.setYRange(-self.VoltsPerBit*128/1000, self.VoltsPerBit*128/1000)
        self.pw1.setTitle(title=self.ui.ADC1Input_cb.currentText())
        self.pw1.setLabel('bottom',text='Time',units='s')
        self.pw1.showLabel('bottom',show=True)
        self.pw1.setLabel('left',text='Voltage', units='V')
        self.pw1.showLabel('left',show=True)
        self.pw1.showGrid(x=True,y=True)

        #Set up the second plot
        self.pw2 = self.ui.graphicsView_2.getPlotItem()
        self.p2 = self.pw2.plot()
        self.p2.setPen((50,200,0))
        self.pw2.setXLink(self.pw2)
        self.pw2.setXRange(0, self.WindowSize/(self.SampleRate))
        self.pw2.setYRange(-self.VoltsPerBit*128/1000, self.VoltsPerBit*128/1000)
        self.pw2.setTitle(title=self.ui.ADC2Input_cb.currentText())
        self.pw2.setLabel('bottom',text='Time',units='s')
        self.pw2.showLabel('bottom',show=True)
        self.pw2.setLabel('left',text='Voltage',units='V')
        self.pw2.showLabel('left',show=True)
        self.pw2.showGrid(x=True,y=True)

        #Set up the third plot
        self.pw3 = self.ui.graphicsView_3.getPlotItem()
        self.p3 = self.pw3.plot()
        self.p3.setPen((100,200,200))
        self.pw3.setXLink(self.pw1)
        self.pw3.setXRange(0, self.WindowSize/(self.SampleRate))
        self.pw3.setYRange(-self.VoltsPerBit*128/1000, self.VoltsPerBit*128/1000)
        self.pw3.setTitle(title=self.ui.ADC3Input_cb.currentText())
        self.pw3.setLabel('bottom',text='Time',units='s')
        self.pw3.showLabel('bottom',show=True)
        self.pw3.setLabel('left',text='EMG',units='V')
        self.pw3.showLabel('left',show=True)
        self.pw3.showGrid(x=True,y=True)

        #Set up the fourth plot
        self.pw4 = self.ui.graphicsView_4.getPlotItem()
        self.p4 = self.pw4.plot()
        self.p4.setPen((200,0,200))
        self.pw4.setXLink(self.pw1)
        self.pw4.setXRange(0, self.WindowSize/(self.SampleRate))
        self.pw4.setYRange(-self.VoltsPerBit*128/1000, self.VoltsPerBit*128/1000)
        self.pw4.setTitle(title=self.ui.ADC4Input_cb.currentText())
        self.pw4.setLabel('bottom',text='Time',units='s')
        self.pw4.showLabel('bottom',show=True)
        self.pw4.setLabel('left',text='EMG',units='V')
        self.pw4.showLabel('left',show=True)
        self.pw4.showGrid(x=True,y=True)

    def connectActions(self):
        self.ui.StimWaveform_cb.currentIndexChanged.connect(self.myStimWaveformComboBoxChanged)
        self.ui.Bionode_cb.currentIndexChanged.connect(self.myBionodeComboBoxChanged)
        self.ui.Record_b.clicked.connect(self.myRecordButtonClicked)
        self.ui.Update_b.clicked.connect(self.myUpdateButtonClicked)
        self.ui.linkAxes_cb.stateChanged.connect(self.myLinkAxesCheckBoxChecked)
        self.ui.StartStim_b.clicked.connect(self.myStartStimButtonClicked)
        self.ui.StopStim_b.clicked.connect(self.myStopStimButtonClicked)
        self.ui.Connect_b.clicked.connect(self.myConnectButtonClicked)
        self.ui.UpdateBaseStation_b.clicked.connect(self.myUpdateBaseStationButtonClicked)
        self.ui.PowerAmplifierOn_cb.stateChanged.connect(self.myPowerAmplifierOnCheckBoxChecked)
        self.ui.Shutdown_b.clicked.connect(self.myShutDownButtonClicked)
        self.ui.PairBaseStation_b.clicked.connect(self.myPairBaseStationButtonClicked)
        self.ui.StartImpedanceTest_b.clicked.connect(self.myStartImpedanceTestButtonClicked)
        self.ui.StopImpedanceTest_b.clicked.connect(self.myStopImpedanceTestButtonClicked)
        self.ui.AddIP_b.clicked.connect(self.myIPAddButtonClicked)
        self.ui.PauseDisplay_b.clicked.connect(self.myPauseDisplayButtenClicked)
        self.ui.Enable4Channels_cb.stateChanged.connect(self.myEnable4ChannelsCheckBoxChecked)
        self.ui.AutoFrequencyOn_cb.stateChanged.connect(self.myAutoFrequencyCheckBoxChecked)
        self.stimTimer.timeout.connect(lambda: self.updateStimTimerGui(1))
        self.cdTimer.timeout.connect(self.updateCdTimerGui)
        self.ui.StartTimer_b.clicked.connect(self.myStartTimerButtonClicked)
        self.ui.StopTimer_b.clicked.connect(self.myStopTimerButtonClicked)
        self.ui.RunImpedanceTest_b.clicked.connect(self.myRunImpedanceTestButtonClicked)

    def updatePlotVisibility(self,numberOfPlots):
        if (numberOfPlots == 2):
            self.ui.graphicsView_1.setGeometry(QtCore.QRect(10, 260, 871, 201))
            self.ui.graphicsView_2.setGeometry(QtCore.QRect(10, 470, 871, 201))
            self.ui.graphicsView_3.setVisible(False)
            self.ui.graphicsView_4.setVisible(False)
        else:
            self.ui.graphicsView_1.setGeometry(QtCore.QRect(10, 260, 431, 201))
            self.ui.graphicsView_2.setGeometry(QtCore.QRect(10, 470, 431, 201))
            self.ui.graphicsView_3.setVisible(True)
            self.ui.graphicsView_4.setVisible(True)

    def myLinkAxesCheckBoxChecked(self):
        if self.ui.linkAxes_cb.isChecked():
            self.pw1.setXLink(self.pw1)
            self.pw2.setXLink(self.pw1)
            self.pw3.setXLink(self.pw1)
            self.pw4.setXLink(self.pw1)
        else:
            self.pw1.setXLink(self.pw1)
            self.pw2.setXLink(self.pw2)
            self.pw3.setXLink(self.pw3)
            self.pw4.setXLink(self.pw4)

    def myStimWaveformComboBoxChanged(self):
        if (self.ui.StimWaveform_cb.currentText() == "Alternating Phase"):
            self.ui.InterphasicDelay_te.setVisible(False)
            self.ui.InterphasicDelayUnits_l.setVisible(False)
            self.ui.InterphasicDelay_l.setVisible(False)
            self.ui.PulsePeriod_l.setText('Pulse Period')
        else:
            self.ui.InterphasicDelay_te.setVisible(True)
            self.ui.InterphasicDelayUnits_l.setVisible(True)
            self.ui.InterphasicDelay_l.setVisible(True)
            self.ui.PulsePeriod_l.setText('Stimulation Period')

    def myBionodeComboBoxChanged(self):
        self.ui.BaseStationAddress_cb.removeItem(0)
        self.ui.BaseStationAddress_cb.addItem(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['bs_ip'])
        
        self.ui.BaseStationAddress_cb.setCurrentIndex(0)

    def myAutoFrequencyCheckBoxChecked(self,state):
        if (state == 2):
            self.ui.ChargeFrequency_dsb.setEnabled(False)
            self.ui.label_13.setEnabled(False)
            self.ui.label_14.setEnabled(False)
        else:
            self.ui.ChargeFrequency_dsb.setEnabled(True)
            self.ui.label_13.setEnabled(True)
            self.ui.label_14.setEnabled(True)

    def myRunImpedanceTestButtonClicked(self):
        if (not self.impedanceTestRunning):
            self.ui.StartStim_b.setEnabled(False)
            self.impedanceBuff = [0 for i in range(len(self.impedanceBuff))]
            self.ui.RunImpedanceTest_b.setText('Stop Imp Test')
            self.impedanceTestRunning = True
            self.UpdateBionodeRegistersFromUI(True,False, False, False, False)
        else:
            self.ui.StartStim_b.setEnabled(True)
            self.ui.RunImpedanceTest_b.setText('Start Imp Test')
            self.impedanceTestRunning = False
            self.UpdateBionodeRegistersFromUI(False,True, False, False, False)

    def myEnable4ChannelsCheckBoxChecked(self,state):

        #Consider changing plot visibility only after the update button is pressed
        if (state == 2):
            self.ui.ADC_3_l.setVisible(True)
            self.ui.ADC_4_l.setVisible(True)
            self.ui.ADC3Input_cb.setVisible(True)
            self.ui.ADC4Input_cb.setVisible(True)
            self.ui.ADCSampleRatio_cb.setEnabled(False)
            self.ui.ADCSampleRatio_l.setEnabled(False)
            self.ui.ADCSampleRatio_cb.setCurrentIndex(0)

            for index in range(self.ui.SampleFrequency_cb.count()):
                self.ui.SampleFrequency_cb.setItemText(index, self.SampleFrequencyStrings_4ch[index])

        else:
            self.ui.ADC_3_l.setVisible(False)
            self.ui.ADC_4_l.setVisible(False)
            self.ui.ADC3Input_cb.setVisible(False)
            self.ui.ADC4Input_cb.setVisible(False)
            self.ui.ADCSampleRatio_cb.setEnabled(True)
            self.ui.ADCSampleRatio_l.setEnabled(True)
            self.ui.ADCSampleRatio_cb.setCurrentIndex(0)

            for index in range(self.ui.SampleFrequency_cb.count()):
                self.ui.SampleFrequency_cb.setItemText(index, self.SampleFrequencyStrings_2ch[index])

    def myPauseDisplayButtenClicked(self):
        if (self.displayPaused == False):
            self.displayPaused = True
            self.ui.PauseDisplay_b.setText('Resume Display')
        else:
            self.displayPaused = False
            self.ui.PauseDisplay_b.setText('Pause Display')
        # self._filter = Filter()
        # self.ui.ChargeFrequency_te.installEventFilter(self._filter)

    def myIPAddButtonClicked(self):

        text, ok = QtGui.QInputDialog.getText(self, "Add a Base Station", "Enter Base Station IP Address:", QtGui.QLineEdit.Normal,'192.168.42.x')

        if ok and text != '':
            self.ui.BaseStationAddress_cb.addItem(text)
            self.ui.BaseStationAddress_cb.setCurrentIndex(self.ui.BaseStationAddress_cb.count()-1)

    def myPairBaseStationButtonClicked(self):
        while (self.ui.BaseStationAddress_cb.count() > 0):
            self.ui.BaseStationAddress_cb.removeItem(self.ui.BaseStationAddress_cb.count()-1)
        addresses = []
        print("Listening for incoming messages...")
        UDP_PORT = 9000
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
        sock.setblocking(0)
        sock.bind(('', UDP_PORT))

        ready = select.select([sock], [], [], 2)

        baseStationFound = False
        baseStationAddress = '<emulation>'

        startTime = time.time()

        while (time.time() - startTime) < 5:
            if (ready[0]):
                newData, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
                ready = select.select([sock], [], [], 2)
                #uncomment for it to work!
                if len(newData) > 0:
                    i = 0
                    validPackets = []
                    for p in range(int(len(newData)/BaseStation.SHORT_PACKET_LENGTH)):
                        rawData = newData[i:i+BaseStation.SHORT_PACKET_LENGTH]
                        i += BaseStaiton.SHORT_PACKET_LENGTH

                        packet = BaseStation.GetPacket(rawData, BaseStation.CRCPoly)

                        if (packet.valid):
                            validPackets.append(packet)
                        else:
                            print('Invalid Data Packet!')

                    #Route all valid packets to their correct positions
                    for p in validPackets:
                        if p.PacketType == BaseStation.DataPacketType.IDResponse:
                            baseStationFound = True
                            baseStationAddress = addr[0]
                            if not(addr[0] in addresses):
                                addresses.append(addr[0])

        for a in addresses:
            self.ui.BaseStationAddress_cb.addItem(str(a))

    def myShutDownButtonClicked(self):
        self.BaseStationController.SendShutDownPacketToBaseStation()

    def myPowerAmplifierOnCheckBoxChecked(self,state):
        if (state == 2):
            reply = QtGui.QMessageBox.warning(self, 'Message', 'Are you sure you want to turn on the Power Amplifier?', QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)

            if reply == QtGui.QMessageBox.Yes:
                pass
            else:
                self.ui.PowerAmplifierOn_cb.setCheckState(0)

        else:
            pass

    def myUpdateBaseStationButtonClicked(self):
        self.UpdateBaseStationRegistersFromUI(False)

    def myConnectButtonClicked(self):
         #Create the Base Station Controller
        self.UDP_TX_Port = 9001
        self.UDP_RX_Port = 9000
        self.UDP_RX_IP = ''
        self.UDP_TX_IP = self.ui.BaseStationAddress_cb.currentText()
        if (self.UDP_TX_IP == '<emulation>') or (self.UDP_TX_IP == '<emulate>'):
            self.UDP_TX_IP = '127.0.0.1'

        self.BaseStationController = BaseStation.BaseStationController(BaseStation.BytesPerBSPacket, BaseStation.CRCPoly, self.UDP_TX_Port, self.UDP_RX_Port, self.UDP_TX_IP, self.UDP_RX_IP)

        self.socketsOpened = True

        #If dataview config file exists, read it in and populate the Bionode combo box
        #Otherwise, leave as default values
        if (os.path.isfile(self.DataViewConfigSettingsPath)):
            self.updateParametersFromConfigFile()
        #Update BaseStation Registers. Do not reset the timestamp counter
        self.UpdateBaseStationRegistersFromUI(False)

        if (float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['version']) > 4.1):
            self.ADCrefVoltage = 1.8
            self.SampleFrequencyStrings_4ch.extend(self.SampleFrequencyStrings_4ch_LargePacket)
            self.SampleFrequencyStrings_2ch.extend(self.SampleFrequencyStrings_2ch_LargePacket)
            if (self.ui.Enable4Channels_cb.isChecked()):
                self.ui.SampleFrequency_cb.addItems(self.SampleFrequencyStrings_4ch_LargePacket)
                print('sampleFreq: ', self.SampleFrequencyStrings_4ch)
            else:
                self.ui.SampleFrequency_cb.addItems(self.SampleFrequencyStrings_2ch_LargePacket)
                print('sampleFreq: ', self.SampleFrequencyStrings_2ch)
        else:
            self.ADCrefVoltage = 1.2

        #Kick off datalistener thread
        self.thread = threading.Thread(target=self.data_listener)
        self.thread.daemon = True
        self.thread.start()

        #Start plotTimer
        self.plotTimer.start(UpdatePlotPeriod)

        self.ui.Connections_gb.setEnabled(False)
        self.ui.Connect_b.setEnabled(True)
        self.ui.BaseStationParameters_gb.setEnabled(True)
        self.ui.BionodeRecordingParameters_gb.setEnabled(True)
        self.ui.BionodeStimulusParameters_gb.setEnabled(True)
        
        self.ui.PositiveCalibration_te.setEnabled(True)
        self.ui.NegativeCalibration_te.setEnabled(True)
        self.ui.AnodicCalibration_te.setEnabled(True)
        self.ui.CathodicCalibration_te.setEnabled(True)

        self.ui.Pot1Value_te.setEnabled(True)
        self.ui.Pot2Value_te.setEnabled(True)
        self.ui.ReadPressure_cb.setEnabled(True)
        self.ui.Enable4Channels_cb.setEnabled(True)
        self.ui.BatteryOnOff_cb.setEnabled(True)

        self.ui.PAPower_cb.setEnabled(True)
        self.ui.ChargeFrequency_dsb.setEnabled(True)

        self.ui.StartStim_b.setEnabled(True)
        self.ui.label_60.setEnabled(True)
        self.ui.label_37.setEnabled(True)
        self.ui.label_40.setEnabled(True)
        self.ui.label_36.setEnabled(True)
        self.ui.TimerHr_sb_2.setEnabled(True)
        self.ui.TimerMin_sb_2.setEnabled(True)
        self.ui.TimerSec_sb_2.setEnabled(True)
        self.ui.RunImpedanceTest_b.setEnabled(True)
        self.ui.PauseDisplay_b.setEnabled(True)
        self.ui.Record_b.setEnabled(True)

        self.ui.displayTime_te.setEnabled(True)
        self.ui.label_30.setEnabled(True)
        self.ui.linkAxes_cb.setEnabled(True)
        self.ui.calculateImp_cb.setEnabled(False)

        self.updatePlotLabelsFromConfigFile()

        self.UpdateBionodeRegistersFromUI(False, False, False, False, True)


    def updateParametersFromConfigFile(self):
        deviceOptionsExist = False
        self.dataViewConfig = configparser.ConfigParser()
        self.dataViewConfig.read(self.DataViewConfigSettingsPath)

        for key in self.dataViewConfig.sections():
            if (key == self.ui.Bionode_cb.currentText().lstrip(' ')):
                deviceOptions = key
                deviceOptionsExist = True
                break
        if not(deviceOptionsExist):
            for key in self.dataViewConfig.sections():
                if key.endswith('*') and (self.ui.Bionode_cb.currentText().startswith( key[:-1])):
                    deviceOptions = key.lstrip(' ')
                    deviceOptionsExist = True
        if not(deviceOptionsExist):
            deviceOptions = '<default>'

        if (deviceOptionsExist):
            print('Using Settings for', deviceOptions)
            try:
                #Setup default values and positions of UI elements
                self.ui.PulseAmplitude_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['pulseamplitude'])
                self.ui.PulseWidth_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['pulsewidth'])
                self.ui.PulsePeriod_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['pulseperiod'])
                self.ui.InterphasicDelay_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['interphasicdelay'])
                self.ui.NumStimCycles_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['numstimcycles'])
                if (self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['continuousstim'].strip().lower() == 'true'):
                    self.ui.ContinuousStimulaiton_cb.setChecked(True)
                elif (self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['continuousstim'].strip().lower() == 'false'):
                    self.ui.ContinuousStimulaiton_cb.setChecked(False)
                self.ui.ShundownThres_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['shutdownthresh'])
                self.ui.StimOnTime_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['stimontime'])
                self.ui.StimOffTime_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['stimofftime'])

                if (self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['stimwaveform'] == 'a'):
                    self.ui.StimWaveform_cb.setCurrentIndex(0)
                    self.ui.InterphasicDelay_te.setVisible(False)
                    self.ui.InterphasicDelayUnits_l.setVisible(False)
                    self.ui.InterphasicDelay_l.setVisible(False)
                    self.ui.PulsePeriod_l.setText('Pulse Period')
                elif (self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['stimwaveform'] == 'b'):
                    self.ui.StimWaveform_cb.setCurrentIndex(1)
                    self.ui.InterphasicDelay_te.setVisible(True)
                    self.ui.InterphasicDelayUnits_l.setVisible(True)
                    self.ui.InterphasicDelay_l.setVisible(True)
                    self.ui.PulsePeriod_l.setText('Stim Period')

                self.ADCChannelRatio = self.ui.ADCSampleRatio_cb.currentText()

                # Set ADC Resolution
                found = False
                for i in range(self.ui.ADCResolution_cb.count()):
                    if (self.ui.ADCResolution_cb.itemText(i).strip().split(' ')[0] == self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['adcresolution'].strip().split(' ')[0]):
                        self.ui.ADCResolution_cb.setCurrentIndex(self.ui.ADCResolution_cb.findText(self.ui.ADCResolution_cb.itemText(i)))
                        found = True
                if not found:
                    self.ui.ADCResolution_cb.setCurrentIndex(0)

                # Decide 2 or 4 Channels
                # Note, numChan intentionally reversed because of myEnable4ChannelsCheckBoxChecked() function
                numChan = 2
                if (self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['fourchannel'].strip().lower()[0] == 't'):
                    self.ui.Enable4Channels_cb.setChecked(True)
                    numChan = 2
                elif (self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['fourchannel'].strip().lower()[0] == 'f'):
                    self.ui.Enable4Channels_cb.setChecked(False)
                    numChan = 4
                else:
                    self.ui.Enable4Channels_cb.setChecked(True)
                    numChan = 2
                self.myEnable4ChannelsCheckBoxChecked(numChan)

                # Set sample Frequency
                found = False
                for i in range(self.ui.SampleFrequency_cb.count()):
                    if (self.ui.SampleFrequency_cb.itemText(i).strip().split(' ')[0] == self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['samplefreq'].strip().split(' ')[0]):
                        self.ui.SampleFrequency_cb.setCurrentIndex(self.ui.SampleFrequency_cb.findText(self.ui.SampleFrequency_cb.itemText(i)))
                        found = True
                if not found:
                    self.ui.SampleFrequency_cb.setCurrentIndex(0)

                # ADC1 Channel
                found = False
                for i in range(self.ui.ADC1Input_cb.count()):
                    if (self.ui.ADC1Input_cb.itemText(i).strip().lower() == self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['adc1input'].strip().lower()):
                        self.ui.ADC1Input_cb.setCurrentIndex(self.ui.ADC1Input_cb.findText(self.ui.ADC1Input_cb.itemText(i)))
                        found = True
                if not found:
                    self.ui.ADC1Input_cb.setCurrentIndex(0)
                # ADC2 Channel
                found = False
                for i in range(self.ui.ADC2Input_cb.count()):
                    if (self.ui.ADC2Input_cb.itemText(i).strip().lower() == self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['adc2input'].strip().lower()):
                        self.ui.ADC2Input_cb.setCurrentIndex(self.ui.ADC2Input_cb.findText(self.ui.ADC2Input_cb.itemText(i)))
                        found = True
                if not found:
                    self.ui.ADC2Input_cb.setCurrentIndex(1)

                if (self.ui.Enable4Channels_cb.checkState()):
                    # ADC3 Channel
                    found = False
                    for i in range(self.ui.ADC3Input_cb.count()):
                        if (self.ui.ADC3Input_cb.itemText(i).strip().lower() == self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['adc3input'].strip().lower()):
                            self.ui.ADC3Input_cb.setCurrentIndex(self.ui.ADC3Input_cb.findText(self.ui.ADC3Input_cb.itemText(i)))
                            found = True
                    if not found:
                        self.ui.ADC3Input_cb.setCurrentIndex(2)
                    # ACD4 Channel
                    found = False
                    for i in range(self.ui.ADC4Input_cb.count()):
                        if (self.ui.ADC4Input_cb.itemText(i).strip().lower() == self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['adc4input'].strip().lower()):
                            self.ui.ADC4Input_cb.setCurrentIndex(self.ui.ADC4Input_cb.findText(self.ui.ADC4Input_cb.itemText(i)))
                            found = True
                    if not found:
                        self.ui.ADC4Input_cb.setCurrentIndex(3)

                self.ui.PositiveCalibration_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['poscalib'])
                self.ui.NegativeCalibration_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['negcalib'])
                self.ui.CathodicCalibration_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['cathcalib'])
                self.ui.AnodicCalibration_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['anodcalib'])
                self.ui.ZeroCalibration_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['zerocalib'])
                self.ui.Pot1Value_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['pot1value'])
                self.ui.Pot2Value_te.setText(self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['pot2value'])

                # Battery enables
                if (self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['batteryon'].strip().lower() == 'true'):
                    self.ui.BatteryOnOff_cb.setChecked(True)
                elif (self.ConfigSectionMap(self.dataViewConfig,deviceOptions)['batteryon'].strip().lower() == 'false'):
                    self.ui.BatteryOnOff_cb.setChecked(False)
            except KeyError:
                print('ERROR: some fields of DataViewConfig File are missing.')

    def updatePlotLabelsFromConfigFile(self):
        #Try to get ADC labels here!
        try:
            ADC1Label = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['rec1_label']
            ADC1Units = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['rec1_units']
        except:
            ADC1Label = 'Voltage'
            ADC1Units = 'V'
        try:
            ADC2Label = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['rec2_label']
            ADC2Units = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['rec2_units']
        except:
            ADC2Label = 'Voltage'
            ADC2Units = 'V'
        try:
            ADC3Label = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['opt1_label']
            ADC3Units = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['opt1_units']
        except:
            ADC3Label = 'Voltage'
            ADC3Units = 'V'
        try:
            ADC4Label = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['opt2_label']
            ADC4Units = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['opt2_units']
        except:
            ADC4Label = 'Voltage'
            ADC4Units = 'V'
        try:
            ADC5Label = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['rsh_label']
            ADC5Units = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['rsh_units']
        except:
            ADC5Label = 'Voltage'
            ADC5Units = 'V'
        try:
            ADC6Label = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['rsl_label']
            ADC6Units = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['rsl_units']
        except:
            ADC6Label = 'Voltage'
            ADC6Units = 'V'

        self.ADCLabels[1] = ADC1Label
        self.ADCLabels[2] = ADC2Label
        self.ADCLabels[3] = ADC3Label
        self.ADCLabels[4] = ADC4Label
        self.ADCLabels[5] = ADC5Label
        self.ADCLabels[6] = ADC6Label

        self.ADCUnits[1] = ADC1Units
        self.ADCUnits[2] = ADC2Units
        self.ADCUnits[3] = ADC3Units
        self.ADCUnits[4] = ADC4Units
        self.ADCUnits[5] = ADC5Units
        self.ADCUnits[6] = ADC6Units

        for i in range(len(self.ADCUnits)):
            if (self.ADCUnits[i] == 'N/A'):
                self.ADCUnits[i] = ''

    def myStopStimButtonClicked(self):
        self.stimTimer.stop()
        self.stimCurrentLimitCnt=0
        self.currentLimitCountDown =60
        self.UpdateBionodeRegistersFromUI(False,True,False,False, False)
        self.ui.StartStim_b.setEnabled(True)
        self.ui.StopStim_b.setEnabled(False)
        self.ui.RunImpedanceTest_b.setEnabled(True)

    def myStartStimButtonClicked(self):
        self.stimTimer.start(1000)
        self.UpdateBionodeRegistersFromUI(True,False, False, False, False)
        self.ui.StartStim_b.setEnabled(False)
        self.ui.StopStim_b.setEnabled(True)
        self.ui.RunImpedanceTest_b.setEnabled(False)

    def myUpdateButtonClicked(self):
        self.UpdateBionodeRegistersFromUI(False, False, False, False, True)

    def myStartImpedanceTestButtonClicked(self):
        self.UpdateBionodeRegistersFromUI(False, False, True, False, False)

    def myStopImpedanceTestButtonClicked(self):
        self.UpdateBionodeRegistersFromUI(False,False, False, True, False)

    def myStopTimerButtonClicked(self):
        self.cdTimer.stop()
        self.ui.StartTimer_b.setEnabled(True)
        self.ui.StopTimer_b.setEnabled(False)

    def myStartTimerButtonClicked(self):
        self.cdTimer.start(1000)
        self.ui.StartTimer_b.setEnabled(False)
        self.ui.StopTimer_b.setEnabled(True)

    def getSampleRateFromComboBox(self):
        totalSampleRateString = self.ui.SampleFrequency_cb.currentText()
        if (self.ui.Enable4Channels_cb.isChecked()):
            totalSampleRate = self.SampleFrequencyStringMap[self.ui.SampleFrequency_cb.currentText()]*4
        else:
            totalSampleRate = self.SampleFrequencyStringMap[self.ui.SampleFrequency_cb.currentText()]*2

        return totalSampleRate

    def UpdateBaseStationRegistersFromUI(self, TimeStampReset):
        #Get the current com frequency setting for the bionode

        commFrequency = int(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['commfrequency'])
        PAPowerRegister = self.PAPowerRegisters[self.ui.PAPower_cb.currentText()]

        AutoChargeFrequency = self.ui.AutoFrequencyOn_cb.checkState() == 2
        ChargeFrequency = self.ui.ChargeFrequency_dsb.value()

        if (ChargeFrequency < 330):
            return
        if (ChargeFrequency > 350):
            return
        if(self.ui.PowerAmplifierOn_cb.checkState() == 0):
            PAEnable = False
        else:
            PAEnable = True

        #Make sure the charge frequency is not too low or too high.
        #Add a pop-up error message or something to alert the user that it's not working.


        self.BaseStationController.UpdateBSRegisters(commFrequency, PAPowerRegister, ChargeFrequency, AutoChargeFrequency, PAEnable, TimeStampReset)

    def updateStimTimerGui(self, BionodeNumber):
        #print("updating gui")
        self.currentLimitCountDown -= 1
        if self.currentLimitCountDown ==0: #Reset the current limit counter every minute
            self.stimCurrentLimitCnt = 0
            self.currentLimitCountDown =60

        displayedData=self.p1.getData()
       # print(max(displayedData))
        pk2pkCurrent = max(displayedData[1])-min(displayedData[1])
        self.ui.currentMeas_te.setText("{0:.1f}".format(pk2pkCurrent*1000000/2))
        if (pk2pkCurrent/2)>(float(self.ui.ShundownThres_te.toPlainText())*float(self.ui.PulseAmplitude_te.toPlainText())/1000000):
            self.stimCurrentLimitCnt += 1
            print(self.stimCurrentLimitCnt, self.currentLimitCountDown )
        if self.stimCurrentLimitCnt >=10:
            fileName = self.ui.Bionode_cb.currentText() + '_stimCurrentLimitLog.txt'
            f = open(fileName, 'a')
            f.write("Stim current limit reached at: " + str(datetime.datetime.now()) + "\n")
            f.close()
            print ("Stim current limit reached, stopping stim")
            self.stimCurrentLimitCnt = 0
            
            #self.myStopStimButtonClicked()

        if BionodeNumber == 1:
            Hr = self.ui.TimerHr_sb_1.value()
            Min = self.ui.TimerMin_sb_1.value()
            Sec = self.ui.TimerSec_sb_1.value()
            if (Sec == 0):
                Sec = 59
                if (Min == 0):
                    Min = 59
                    Hr = Hr - 1
                else:
                    Min = Min -1
            else:
                Sec = Sec - 1
            if (Hr == 0)and (Min == 0)and(Sec == 0):
                print("Timer stop!!!")
                self.stimTimer.stop()
                self.autoStopStim = True
                self.stopStimTimer = time.time()
                self.UpdateBionodeRegistersFromUI(False,True,False,False, False)
                self.ui.StartStim_b.setEnabled(True)
                self.ui.StopStim_b.setEnabled(False)
                self.ui.RunImpedanceTest_b.setEnabled(True)

            self.ui.TimerHr_sb_1.setValue(Hr)
            self.ui.TimerMin_sb_1.setValue(Min)
            self.ui.TimerSec_sb_1.setValue(Sec)

    def updateCdTimerGui(self):
        #print("updating cdTimer gui")

        #print(StimOnCounter,StimOffCounter)  
        Hr = self.ui.TimerHr_sb_2.value()
        Min = self.ui.TimerMin_sb_2.value()
        Sec = self.ui.TimerSec_sb_2.value()
        if (Sec == 0):
            Sec = 59
            if (Min == 0):
                Min = 59
                Hr = Hr - 1
            else:
                Min = Min -1
        else:
            Sec = Sec - 1
            
        if (Hr == 0)and (Min < 30)and (self.recording == False):
            self.myRecordButtonClicked()
            
        if (Hr == 0)and (Min == 0)and(Sec == 0):
            print("CDTimer stop!!!")
            self.cdTimer.stop()
            self.myStartStimButtonClicked()
            self.ui.StartTimer_b.setEnabled(True)
            self.ui.StopTimer_b.setEnabled(False)

        self.ui.TimerHr_sb_2.setValue(Hr)
        self.ui.TimerMin_sb_2.setValue(Min)
        self.ui.TimerSec_sb_2.setValue(Sec)



    def UpdateBionodeRegistersFromUI(self, StartStim, StopStim, StartImpedanceTest, StopImpedanceTest,StartThermalTest):
        #Stim parameters
        if (self.impedanceTestRunning):
            #run stim test
            amplitude = 300.0/1000000.0
            pulseRepeatTime = 50000.0/1000000.0
            pulseWidth = 360.0/1000000.0
            numStimCycles = 0xFFFF
            # positiveCalibration = 0
            # negativeCalibration = 0
            # cathodicCalibration = 0
            # anodicCalibration = 0
            # zeroCalibration = 0
            positiveCalibration = int(self.ui.PositiveCalibration_te.toPlainText())
            negativeCalibration = int(self.ui.NegativeCalibration_te.toPlainText())
            cathodicCalibration = int(self.ui.CathodicCalibration_te.toPlainText())
            anodicCalibration = int(self.ui.AnodicCalibration_te.toPlainText())
            zeroCalibration = int(self.ui.ZeroCalibration_te.toPlainText())
            stimOnTime = 0
            stimOffTime = 0
            InterphasicDelay = float(self.ui.InterphasicDelay_te.toPlainText())/1000000
        else:
            try:
                amplitude = float(self.ui.PulseAmplitude_te.toPlainText())/1000000
                pulseRepeatTime = float(self.ui.PulsePeriod_te.toPlainText())/1000000
                pulseWidth = float(self.ui.PulseWidth_te.toPlainText())/1000000
                InterphasicDelay = float(self.ui.InterphasicDelay_te.toPlainText())/1000000
                numStimCycles = int(self.ui.NumStimCycles_te.toPlainText())
                if (self.ui.ContinuousStimulaiton_cb.isChecked()):
                    numStimCycles = 0xFFFF

                positiveCalibration = int(self.ui.PositiveCalibration_te.toPlainText())
                negativeCalibration = int(self.ui.NegativeCalibration_te.toPlainText())
                cathodicCalibration = int(self.ui.CathodicCalibration_te.toPlainText())
                anodicCalibration = int(self.ui.AnodicCalibration_te.toPlainText())
                zeroCalibration = int(self.ui.ZeroCalibration_te.toPlainText())
                stimOnTime = int(self.ui.StimOnTime_te.toPlainText())*32768
                stimOffTime = int(self.ui.StimOffTime_te.toPlainText())*32768
            except:
                print('ERROR, UI input values are not in the correct format')
                return

        totalSampleRate = self.getSampleRateFromComboBox()

        ADCResolutionString = self.ui.ADCResolution_cb.currentText()

        ADCResolution = 0
        if (ADCResolutionString == '8 Bit'):
            ADCResolution = 8
        elif(ADCResolutionString == '10 Bit'):
            ADCResolution = 10

        StartImpedanceTest1 = False
        StopImpedanceTest1 = False
        StartImpedanceTest2 = False
        StopImpedanceTest2 = False

        impedanceTestChString = self.ui.ImpedanceTestCh_cb.currentText()

        if (StartImpedanceTest):
            if (impedanceTestChString == 'Rec 1'):
                StartImpedanceTest1 = True
            else:
                StartImpedanceTest2 = True
        elif (StopImpedanceTest):
            if (impedanceTestChString == 'Rec 1'):
                StopImpedanceTest1 = True
            else:
                StopImpedanceTest2 = True

        self.ADCChannelRatio = self.ui.ADCSampleRatio_cb.currentText()

        ADC1InputCode = self.ADCCodes[self.ui.ADC1Input_cb.currentText()]
        ADC2InputCode = self.ADCCodes[self.ui.ADC2Input_cb.currentText()]

        if (self.ui.Enable4Channels_cb.isChecked()):
            ADC3InputCode = self.ADCCodes[self.ui.ADC3Input_cb.currentText()]
            ADC4InputCode = self.ADCCodes[self.ui.ADC4Input_cb.currentText()]
        else:
            ADC3InputCode = ADC1InputCode
            ADC4InputCode = ADC2InputCode

        if (self.ui.StimWaveform_cb.currentText() == 'Alternating Phase'):
            StimWaveformRegister = 0
        else:
            StimWaveformRegister = 1

        if (self.ui.ReadPressure_cb.isChecked()):
            PressureReadingConfig = 0x1
        else:
            PressureReadingConfig = 0x0

        if (self.ui.BatteryOnOff_cb.isChecked()):
            BatterySwitchConfig = 0x1
        else:
            BatterySwitchConfig  = 0x0    

        version = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['version']

        self.BaseStationController.UpdateBionodeRegisters(version, amplitude, pulseRepeatTime, pulseWidth, numStimCycles, stimOnTime, stimOffTime,totalSampleRate, ADCResolution, StartStim, StopStim, StartImpedanceTest1, StopImpedanceTest1, StartImpedanceTest2, StopImpedanceTest2, StartThermalTest, self.ADCChannelRatio, positiveCalibration, negativeCalibration, cathodicCalibration, anodicCalibration, zeroCalibration, ADC1InputCode, ADC2InputCode, ADC3InputCode, ADC4InputCode, StimWaveformRegister, InterphasicDelay, BatterySwitchConfig)

        self.pw1.setTitle(title=self.ui.ADC1Input_cb.currentText())
        self.pw2.setTitle(title=self.ui.ADC2Input_cb.currentText())
        self.pw3.setTitle(title=self.ui.ADC3Input_cb.currentText())
        self.pw4.setTitle(title=self.ui.ADC4Input_cb.currentText())

        print(self.ADCLabels[self.ADCCodes[self.ui.ADC1Input_cb.currentText()]])

        self.pw1.setLabel('left',text=self.ADCLabels[self.ADCCodes[self.ui.ADC1Input_cb.currentText()]],units=self.ADCUnits[self.ADCCodes[self.ui.ADC1Input_cb.currentText()]])
        self.pw2.setLabel('left',text=self.ADCLabels[self.ADCCodes[self.ui.ADC2Input_cb.currentText()]],units=self.ADCUnits[self.ADCCodes[self.ui.ADC2Input_cb.currentText()]])
        self.pw3.setLabel('left',text=self.ADCLabels[self.ADCCodes[self.ui.ADC3Input_cb.currentText()]],units=self.ADCUnits[self.ADCCodes[self.ui.ADC3Input_cb.currentText()]])
        self.pw4.setLabel('left',text=self.ADCLabels[self.ADCCodes[self.ui.ADC4Input_cb.currentText()]],units=self.ADCUnits[self.ADCCodes[self.ui.ADC4Input_cb.currentText()]])

        resolutionHalfPoint = pow(2, ADCResolution-1)
        self.displayTime = float(self.ui.displayTime_te.toPlainText())

        if (self.ui.Enable4Channels_cb.isChecked()):#self.ui.Enable4Channels_cb.isChecked()):
            self.numberOfChannelsDisplayed = 4
            self.SampleRate = totalSampleRate/4
            self.WindowSize = int(self.displayTime*self.SampleRate)*2
            self.pw1.setXRange(0, self.WindowSize/self.SampleRate/2)
            self.pw2.setXRange(0, self.WindowSize/self.SampleRate/2)
            self.pw3.setXRange(0, self.WindowSize/self.SampleRate/2)
            self.pw4.setXRange(0, self.WindowSize/self.SampleRate/2)

            self.ch1gain = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1gain'])
            self.ch2gain = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2gain'])
            self.ch3gain = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch3gain'])
            self.ch4gain = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch4gain'])

            try:
                self.ch1offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1offset'])
                self.ch2offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2offset'])
                self.ch3offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch3offset'])
                self.ch4offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch4offset'])
            except:
                self.ch1offset = 0.9
                self.ch2offset = 0.9
                self.ch3offset = 0.9
                self.ch4offset = 0.9

            self.pw1.setYRange(-self.ch1offset/self.ch1gain, (self.ADCrefVoltage-self.ch1offset)/self.ch1gain)
            self.pw2.setYRange(-self.ch2offset/self.ch2gain, (self.ADCrefVoltage-self.ch2offset)/self.ch2gain)
            self.pw3.setYRange(-self.ch3offset/self.ch3gain, (self.ADCrefVoltage-self.ch3offset)/self.ch3gain)
            self.pw4.setYRange(-self.ch4offset/self.ch4gain, (self.ADCrefVoltage-self.ch4offset)/self.ch4gain)

            # self.pw1.setYRange(-self.VoltsPerBit*resolutionHalfPoint/self.ch1gain + self.ch1offset/self.ch1gain, self.VoltsPerBit*resolutionHalfPoint/self.ch1gain + self.ch1offset/self.ch1gain)
            # self.pw2.setYRange(-self.VoltsPerBit*resolutionHalfPoint/self.ch2gain + self.ch2offset/self.ch2gain, self.VoltsPerBit*resolutionHalfPoint/self.ch2gain + self.ch2offset/self.ch2gain)
            # self.pw3.setYRange(-self.VoltsPerBit*resolutionHalfPoint/self.ch3gain + self.ch3offset/self.ch3gain, self.VoltsPerBit*resolutionHalfPoint/self.ch3gain + self.ch3offset/self.ch3gain)
            # self.pw4.setYRange(-self.VoltsPerBit*resolutionHalfPoint/self.ch4gain + self.ch4offset/self.ch4gain, self.VoltsPerBit*resolutionHalfPoint/self.ch4gain + self.ch4offset/self.ch4gain)

            self.updatePlotVisibility(4)

        else:
            self.numberOfChannelsDisplayed = 2
            self.SampleRate = totalSampleRate/2
            self.WindowSize = int(self.displayTime*self.SampleRate)
            self.pw1.setXRange(0, self.WindowSize/self.SampleRate)
            self.pw2.setXRange(0, self.WindowSize/self.SampleRate)

            self.ch1gain = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1gain'])
            self.ch2gain = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2gain'])

            try:
                self.ch1offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1offset'])
                self.ch2offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2offset'])
            except:
                self.ch1offset = 0.9
                self.ch2offset = 0.9

            self.pw1.setYRange(-self.ch1offset/self.ch1gain, (self.ADCrefVoltage-self.ch1offset)/self.ch1gain)
            self.pw2.setYRange(-self.ch2offset/self.ch2gain, (self.ADCrefVoltage-self.ch2offset)/self.ch2gain)

            self.updatePlotVisibility(2)

        if (len(self.displayData) > (self.WindowSize*self.numberOfChannelsDisplayed)):
            self.displayData = self.displayData[(len(self.displayData)-self.WindowSize*self.numberOfChannelsDisplayed):]
        elif (len(self.displayData) < self.WindowSize*self.numberOfChannelsDisplayed):
            self.displayData = ([-2]*(self.WindowSize*self.numberOfChannelsDisplayed-len(self.displayData))) + self.displayData


    def createNewRecordingFile(self):
        #First, note the current time and clear the base-station time
        self.recordingFileSize = 0
        self.savedInitiatedTimeStamp = self.prevTimeStamp
        timeStamp = datetime.datetime.now()
        fileName = str(timeStamp.year) + '.' + str(timeStamp.month) +  '.' + str(timeStamp.day) +  '.' + str(timeStamp.hour) +  '.' + str(timeStamp.minute) +  '.' + str(timeStamp.second) + '_' + self.ui.Bionode_cb.currentText()

        #Update the base station registers to indicate that the timestamp should be reset.
        self.UpdateBaseStationRegistersFromUI(True)

        totalSampleRate = self.getSampleRateFromComboBox()

        #Now, make a file and fill in the timestamp information as the top packet
        self.recordingFile = open (fileName + '.bin', "wb")
        tsByteArray = bytearray([timeStamp.year>>8, timeStamp.year & 0xFF, timeStamp.month, timeStamp.day, timeStamp.hour, timeStamp.minute, timeStamp.second,(totalSampleRate>>8)&0xFF, totalSampleRate&0xFF, self.numberOfChannelsDisplayed&0xFF]) 
        self.recordingFile.write(tsByteArray)
        self.recordingFile.write(bytearray([0 for i in range(self.BaseStationController.BytesPerBNodePacket - 10)]))
        self.recording = True
        
        parameterFile = open(fileName + '_parameters.txt', "w")
        parameterFile.write('Date: ' + str(timeStamp.year) + ' ' + str(timeStamp.month) +  ' ' + str(timeStamp.day) + '\r\n')
        parameterFile.write('Time: ' + str(timeStamp.hour) +  ' ' + str(timeStamp.minute) +  ' ' + str(timeStamp.second) + '\r\n')
        parameterFile.write('Bionode: ' + self.ui.Bionode_cb.currentText() + '\r\n')
        parameterFile.write('Base Station IP: ' + self.ui.BaseStationAddress_cb.currentText() + '\r\n')
        parameterFile.write('Sampling Frequency: ' + self.ui.SampleFrequency_cb.currentText() + '\r\n')
        parameterFile.write('ADC Resolution: ' + self.ui.ADCResolution_cb.currentText() + '\r\n')
        parameterFile.write('ADC 1 Input: ' + self.ui.ADC1Input_cb.currentText() + '\r\n')
        parameterFile.write('ADC 2 Input: ' + self.ui.ADC2Input_cb.currentText() + '\r\n')
        parameterFile.write('Pulse Amplitude (uA): ' + self.ui.PulseAmplitude_te.toPlainText() + '\r\n')
        parameterFile.write('Zero Cal.: ' + self.ui.ZeroCalibration_te.toPlainText() + '\r\n')
        parameterFile.write('Pulse Width (us): ' + self.ui.PulseWidth_te.toPlainText() + '\r\n')
        parameterFile.write('Pulse Period (us): ' + self.ui.PulsePeriod_te.toPlainText() + '\r\n')
        parameterFile.write('# Stim Cycles: ' + self.ui.NumStimCycles_te.toPlainText() + '\r\n')
        parameterFile.write('Shutdown Threshold: ' + self.ui.ShundownThres_te.toPlainText() + '\r\n')
        parameterFile.write('Stim ON Time: ' + self.ui.StimOnTime_te.toPlainText() + '\r\n')
        parameterFile.write('Stim OFF Time '+ self.ui.StimOffTime_te.toPlainText() + '\r\n')
        parameterFile.write('Timer Value: ' + self.ui.TimerHr_sb_2.text() + ' hr ' + self.ui.TimerMin_sb_2.text() + ' min ' + self.ui.TimerSec_sb_2.text() + ' sec' + '\r\n')
        parameterFile.write('Stim Time: ' + self.ui.TimerHr_sb_1.text() + ' hr ' + self.ui.TimerMin_sb_1.text() + ' min ' + self.ui.TimerSec_sb_1.text() + ' sec' + '\r\n')
        parameterFile.close()

    def myRecordButtonClicked(self):
        if (self.recording == False):
            self.createNewRecordingFile()
            self.recording = True
            self.ui.Record_b.setText('Stop Rec')

        else:
            self.recording = False
            self.autoStopStim = False
            self.ui.Record_b.setText('Record')

    def data_listener(self):
        ready = select.select([self.BaseStationController.socket_TXRX], [], [])
        newData = []
        addr = -1
        # Look up table to determine packet length from data type
        packetLength = {0:58, 1:58, 2:58, 3:58, 4:58, 5:58, 6:58, 7:58, 8:58, 9:198, 10:198, 255:58}

        self.stoppedSocket = False

        while not self.closing:
            ready = select.select([self.BaseStationController.socket_TXRX], [], [])
            if (ready[0]):
                newData, addr = self.BaseStationController.socket_TXRX.recvfrom(1024) # buffer size is 1024 bytes
                if (addr[0] != self.UDP_TX_IP):
                    newData = []
                if len(newData) > 0:
                    i = 0
                    validPackets = []
                    while (i < len(newData)):
                        try:
                            rawData = newData[ i:i+packetLength[newData[i+6]]]
                        except Exception as e:
                            print('Error unpacking data')
                            break

                        i += packetLength[newData[i+6]]
                        packet = BaseStation.GetPacket(rawData, BaseStation.CRCPoly)
                        if (packet.valid):
                            validPackets.append(packet)
                            #print(packet.PacketType)
                        else:
                            print('Invalid Data Packet!')

                    #Route all valid packets to their correct positions
                    for p in validPackets:
                        #If recording, save all valid packets
                        if self.recording:
                            if self.recordingFileSize > 1000000000:
                                self.recordingFile.close()
                                self.createNewRecordingFile()

                            self.recordingFile.write(bytearray(p.rawData))
                            self.recordingFileSize = self.recordingFileSize + len(p.rawData)

                            if (self.autoStopStim == True) and (time.time() - self.stopStimTimer > self.additionalRecordTime):
                                self.myRecordButtonClicked()
                                self.autoStopStim = False
                                self.recording = False
                                self.ui.Record_b.setText('Record')
                                
                        if (not self.recording) and hasattr(self, 'recordingFile'):
                            self.recordingFile.close()

                        #Update display buffer to display all data packets
                        if (p.PacketType == BaseStation.DataPacketType.Data_8bit_Small) or (p.PacketType == BaseStation.DataPacketType.Data_8bit_Large):
                            self.VoltsPerBit = self.ADCrefVoltage/256
                            self.dataToDisplay.extend(p.DataPoints)
                            
                        elif ((p.PacketType == BaseStation.DataPacketType.Data_10bit_Small) or (p.PacketType == BaseStation.DataPacketType.Data_10bit_Large)):
                            #Update VoltsPerBit
                            self.VoltsPerBit = self.ADCrefVoltage/1024
                            dp = BaseStation.DataPacket(p.rawData, p.CRCPoly, 10)
                            self.dataToDisplay.extend(dp.DataPoints)

                        elif p.PacketType == BaseStation.DataPacketType.Impedance:
                            self.VoltsPerBit = self.ADCrefVoltage/256
                            self.dataToDisplay.extend(p.DataPoints)

                        elif p.PacketType == BaseStation.DataPacketType.Thermal:
                            self.VoltsPerBit = self.ADCrefVoltage/256
                            self.dataToDisplay.extend(p.DataPoints)
                            self.newThermalDataReady = True
                            self.thermalData = (p.ThermalData[0]*256 + p.ThermalData[1])*0.0625

            #if (not self.recording) and hasattr(self, 'recordingFile'):
            #    self.recordingFile.close()

        self.stoppedSocket = True

    def updatePlot(self):
        global displayData, dataToDisplay, newThermalDataReady

        if (self.newThermalDataReady):
            self.ui.ImplantTemperature_te.setText(str(self.thermalData))
            self.newThermalDataReady = False
            #Send packet to Basestation to stop future thermal updates
            self.UpdateBionodeRegistersFromUI(False,False, False, False, False)


        newData = list(self.dataToDisplay)
        self.dataToDisplay = []
        newDisplay = list(self.displayData[len(newData):len(self.displayData)] + newData)
        newImpedance = list(self.impedanceBuff[len(newData):len(self.impedanceBuff)] + newData)
        self.displayData = list(newDisplay)
        self.impedanceBuff = list(newImpedance)
        if len(self.displayData) > (self.WindowSize*2):
            print('overflow!')
            self.displayData = self.displayData[-(self.WindowSize*2):]

        if (self.impedanceTestRunning):
            maxCurrentNumber = max(self.impedanceBuff[0::2])
            maxVoltageNumber = max(self.impedanceBuff[1::2])

            #self.ch1gain = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1gain'])
            #self.ch2gain = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2gain'])

            # topCurrentNums = []
            # currentThreshold = maxCurrentNumber*0.75

            # topVoltageNums = []
            # voltageThreshold = maxVoltageNumber*0.75

            # for c in self.displayData[0::2]:
            #     if c > currentThreshold:
            #         topCurrentNums.append(c)

            # for v in self.displayData[1::2]:
            #     if v > voltageThreshold:
            #         topVoltageNums.append(c)

            # #print('c = ', len(topCurrentNums))
            # #print('v = ', len(topVoltageNums))
            # maxCurrentAmps = sum(topCurrentNums)/len(topCurrentNums)
            # maxVoltageVolts = sum(topVoltageNums)/len(topVoltageNums)

            maxCurrentAmps = (maxCurrentNumber-128)/self.ch1gain
            maxVoltageVolts = (maxVoltageNumber-128)/self.ch2gain
            if (maxCurrentAmps != 0):
                impedance = maxVoltageVolts/maxCurrentAmps/1000 # kΩ
            else:
                impedance = 0

            if (impedance < 24):
                self.ui.impedanceResult_l.setText('{:6.1f}'.format(impedance) + 'kΩ')
                #self.ui.impedanceResult_l.setText('{:6.1f}'.format((max(self.displayData[1::2])/max(self.displayData[::2]))) + ' Ω')
            else:
                self.ui.impedanceResult_l.setText('MAX')

        if not self.displayPaused:

            #self.ch1gain = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1gain'])
            #self.ch2gain = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2gain'])


            if (self.ADCChannelRatio == '1:1'):
                if (self.numberOfChannelsDisplayed == 4):#self.ui.Enable4Channels_cb.isChecked()):
                    #self.ch3gain = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch3gain'])
                    #self.ch4gain = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch4gain'])

                    #try:
                    #    self.ch1offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1offset'])
                    #    self.ch2offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2offset'])
                    #    self.ch3offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch3offset'])
                    #    self.ch4offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch4offset'])
                    #except:
                    #    self.ch1offset = 0.9
                    #    self.ch2offset = 0.9
                    #    self.ch3offset = 0.9
                    #    self.ch4offset = 0.9

                    self.p1.setData(y=[(i*self.VoltsPerBit)/self.ch1gain - self.ch1offset/self.ch1gain for i in self.displayData[::4]], x=[i/self.SampleRate for i in range(int(self.WindowSize/2))])
                    self.p2.setData(y=[(i*self.VoltsPerBit)/self.ch2gain - self.ch2offset/self.ch2gain for i in self.displayData[1::4]], x=[i/self.SampleRate for i in range(int(self.WindowSize/2))])       
                    self.p3.setData(y=[(i*self.VoltsPerBit)/self.ch3gain - self.ch3offset/self.ch3gain for i in self.displayData[2::4]], x=[i/self.SampleRate for i in range(int(self.WindowSize/2))])
                    self.p4.setData(y=[(i*self.VoltsPerBit)/self.ch4gain - self.ch4offset/self.ch4gain for i in self.displayData[3::4]], x=[i/self.SampleRate for i in range(int(self.WindowSize/2))])
                else:
                    #try:
                    #    self.ch1offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1offset'])
                    #    self.ch2offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2offset'])
                    #except:
                    #    self.ch1offset = 0.9
                    #    self.ch2offset = 0.9
                    self.p1.setData(y=[(i*self.VoltsPerBit)/self.ch1gain - self.ch1offset/self.ch1gain for i in self.displayData[::2]], x=[i/self.SampleRate for i in range(int(self.WindowSize))])
                    self.p2.setData(y=[(i*self.VoltsPerBit)/self.ch2gain - self.ch2offset/self.ch2gain for i in self.displayData[1::2]], x=[i/self.SampleRate for i in range(int(self.WindowSize))])
            
            elif (self.ADCChannelRatio == '1:3'):
                #try:
                #    self.ch1offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1offset'])
                #    self.ch2offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2offset'])
                #except:
                #    self.ch1offset = 0.9
                #    self.ch2offset = 0.9
                self.p1.setData(y=[(i*self.VoltsPerBit)/self.ch1gain - self.ch1offset/self.ch1gain for i in self.displayData[::4]], x=[i/self.SampleRate*2 for i in range(len(self.displayData[::4]))])

                ch2DisplayList = list(self.displayData)
                del(ch2DisplayList[::4])

                self.p2.setData(y=[(i*self.VoltsPerBit)/self.ch2gain - self.ch2offset/self.ch2gain for i in ch2DisplayList], x=[i/self.SampleRate*2/3 for i in range(len(ch2DisplayList))])

            elif (self.ADCChannelRatio == '1:39'):
                #try:
                #    self.ch1offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1offset'])
                #    self.ch2offset = float(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2offset'])
                #except:
                #    self.ch1offset = 0.9
                #    self.ch2offset = 0.9
                self.p1.setData(y=[(i*self.VoltsPerBit)/self.ch1gain - self.ch1offset/self.ch1gain for i in self.displayData[::40]], x=[i/self.SampleRate*20 for i in range(len(self.displayData[::40]))])

                ch2DisplayList = list(self.displayData)
                del(ch2DisplayList[::40])

                self.p2.setData(y=[(i*self.VoltsPerBit)/ch2gain - self.ch2offset/self.ch2gain for i in ch2DisplayList], x=[i/self.SampleRate*20/39 for i in range(len(ch2DisplayList))])



class ControlMainWindow(QtGui.QMainWindow, Ui_Form):
    def __init__(self, parent=None):
        #Call parrent initializer function
        super(ControlMainWindow, self).__init__(parent)

        #initialize the empty tab form
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        #Now, remove the first blank tab. We'll be replacing it hhere shortly
        self.ui.tabWidget.removeTab(0)
        self.tabList = [self.ui.AddTab]
        self.createNewTab(0)
        self.ui.tabWidget.setCurrentIndex(0)

        #Connect UI Actions
        self.connectActions()

    def createNewTab(self, index):
        self.tabList.insert(index,BionodeDataView())
        self.tabList[index].setAutoFillBackground(True)
        self.ui.tabWidget.insertTab(index,self.tabList[index], "")
        _translate = QtCore.QCoreApplication.translate
        self.ui.tabWidget.setTabText(self.ui.tabWidget.indexOf(self.tabList[index]), _translate("Form", "Tab "+str(len(self.tabList)-1)))

    def connectActions(self):
        self.ui.tabWidget.currentChanged.connect(self.myTabChanged)

    def myTabChanged(self, index):
        if (index == (self.ui.tabWidget.count()-1)):
            self.createNewTab(index)
            self.ui.tabWidget.setCurrentIndex(index)

if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    mySW = ControlMainWindow()
    mySW.show()
    sys.exit(app.exec_())
