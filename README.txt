======================================
To build for Mac:
=====================================
Run the following command from a MAC terminal. Must have py2app module installed.

python3 setup_py2app.py py2app

Find the application in the dist folder. Navigate into the application by right-clicking on the application and selecting "view package contents." Navigate to the "Resources" directory and copy both the BaseStationConfigSettings.ini and BionodeConfigSettings.ini files into the directory. Back out of the package contents, and then run the program by double-clicking on the application

=====================================
To build for PC:
=====================================
Run the following command from a PC terminal. Must have pyinstaller installed:

pyinstaller BionodeDataView.py -w

The executable is inside the directory created in the dist folder. Copy both the BaseStationConfigSettings.ini and BionodeConfigSettings.ini files into the directory containing the executable, and then double click on the executable to launch the program.

=====================================
To update UI:
=====================================
Be sure to always update the UI using Qt Creator installed on a windows machine. The Windows UI elements are larger than the Mac ones. Updating the UI using a Mac could create UI elements that are crowded on a PC user interface.

To generate a UI python file, run the following command:

pyuic5 -x <input .ui file name> -o <output .py file name>