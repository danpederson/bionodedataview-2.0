from distutils.core import setup
from glob import glob
import py2exe
import sys 

sys.path.append(r'redist\x86')
data_files = [("Microsoft.VC90.CRT", glob(r'redist\x86\*.*'))]

setup(data_files=data_files, windows=['BionodeDataView.py'])
