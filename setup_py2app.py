from setuptools import setup

APP = ['BionodeDataView.py']
DATA_FILES = []
OPTIONS = {'argv_emulation': True, 'includes': ['sip', 'PyQt4.QtGui', 'PyQt4.QtCore', 'pyqtgraph']}

setup(
app=APP,
data_files=DATA_FILES,
options={'py2app': OPTIONS},
setup_requires=['py2app'],
)