from enum import Enum
import socket
import threading
import math

# Sync(2) + PID(2) + Tx(1) + Rx(1) + Type(1) + Data(__) + StimStatus(5) + TimeStamp(4) + CRC(2)
SMALL_PACKET_LENGTH = 58
LARGE_PACKET_LENGTH = 198
BytesPerBSPacket = 58

CRCPoly = 0x8408

class DataPacketType(Enum):
    Data_8bit_Small    = 0
    Data_10bit_Small   = 1
    Impedance          = 2
    Thermal            = 3
    BionodeConfig      = 4
    BaseStationConfig  = 5
    IDResponse         = 6
    IDPing             = 7
    Invalid            = 8
    Data_8bit_Large    = 9
    Data_10bit_Large   = 10
    ShutDown           = 255

class DeviceID(Enum):
    PC                 = 0
    BaseStation        = 1
    Bionode            = 2
    Invalid            = 3

def CRCGen(data, CRCPoly):
    r = 0
    for i in range(len(data)):
        r ^= data[i]
        for j in range(8):
            if (r & 0x0001) == 0x0001:
                r = ((r >> 1) ^ CRCPoly) & 0xFFFF
            else:
                r = (r >> 1) & 0xFFFF
    return r

#def GetDataPoints(payload):
#     return payload[:BytesPerBNodePacket]

class BaseStationRegisters():
    def __init__(self):
        CommFrequency = 0x00
        ResetTimestampTimer = 0x00
        CoilDrive = 0x00

class Packet():
    def __init__(self, rawData, CRCPoly):
        PacketPayloadOffset = 7 # Sync(2) + PID(2) + Tx(1) + Rx(1) + Type(1)

        self.Sync = (rawData[0]<<8) + rawData[1]
        self.PID = (rawData[2]<<8) + rawData[3]
        self.TXID = GetDeviceID(rawData[4])
        self.RXID = GetDeviceID(rawData[5])
        self.PacketType = GetPacketType(rawData[6])

        if (self.PacketType == DataPacketType.Data_8bit_Large) or (self.PacketType == DataPacketType.Data_10bit_Large):
            self.BytesPerBNodePacket = LARGE_PACKET_LENGTH
        else:
            self.BytesPerBNodePacket = SMALL_PACKET_LENGTH
        self.StimStatusLength = 5

        # Sync(2) + PID(2) + Tx(1) + Rx(1) + Type(1) + Data(__) + StimStatus(5) + TimeStamp(4) + CRC(2)
        self.PacketPayloadLength = self.BytesPerBNodePacket - self.StimStatusLength  - 13

        self.Payload = rawData[PacketPayloadOffset:self.BytesPerBNodePacket-2]
        self.CRC = rawData[self.BytesPerBNodePacket-2]<<8 + rawData[self.BytesPerBNodePacket-1]
        self.rawData = rawData
        self.valid = CRCGen(rawData[2:],CRCPoly) == 0
        self.Timestamp = self.Payload[-1] + self.Payload[-2]*math.pow(2,8)+self.Payload[-3]*math.pow(2,16)+self.Payload[-4]*math.pow(2,24)
        self.CRCPoly = CRCPoly

class DataPacket(Packet):
    def __init__(self, rawData, CRCPoly, resolution):
        super(DataPacket,self).__init__(rawData, CRCPoly)
        if (resolution == 8):
            self.DataPoints = self.Payload[:self.PacketPayloadLength]
        else:
            x = self.Payload[:self.PacketPayloadLength]
            y = [0] * int(self.PacketPayloadLength / 5)
            for i in range(len(y)):
                y[i] = (x[5*i]<<32) | (x[5*i+1]<<24) | (x[5*i+2]<<16) | (x[5*i+3]<<8) | (x[5*i+4])

            z=[]
            for j in range(len(y)):
                mask = 0xFFC0000000
                for k in range(4):
                    z.append((y[j]&mask)>>(30-k*10))
                    mask = mask>>10

            self.DataPoints = z

        self.StimStatus = self.Payload[self.PacketPayloadLength+1 : self.PacketPayloadLength+self.StimStatusLength]

class ImpedancePacket(Packet):
    def __init__(self, rawData, CRCPoly):
        super(ImpedancePacket,self).__init__(rawData, CRCPoly)
        self.DataPoints = self.Payload[:self.PacketPayloadLength]

class ThermalPacket(Packet):
    def __init__(self, rawData, CRCPoly):
        super(ThermalPacket,self).__init__(rawData, CRCPoly)
        self.DataPoints = self.Payload[:self.PacketPayloadLength]
        self.ThermalData = self.Payload[self.PacketPayloadLength:self.PacketPayloadLength+2]

class BionodeConfigPacket(Packet):
    def __init__(self, rawData, CRCPoly):
        super(BionodeConfigPacket,self).__init__(rawData, CRCPoly)

class BaseStationConfigPacket(Packet):
    def __init__(self, rawData, CRCPoly):
        super(BaseStationConfigPacket,self).__init__(rawData, CRCPoly)

class IDResponsePacket(Packet):
    def __init__(self, rawData, CRCPoly):
        super(IDResponsePacket,self).__init__(rawData, CRCPoly)

class IDPingPacket(Packet):
    def __init__(self, rawData, CRCPoly):
        super(IDPingPacket,self).__init__(rawData, CRCPoly)

def GetPacketType(dataPacketTypeNumber):
    dpt = DataPacketType.Invalid
    try:
        dpt = DataPacketType(dataPacketTypeNumber)
    except:
        dpt = DataPacketType.Invalid
    return dpt

def GetDeviceID(deviceIDNumber):
    deviceID = DeviceID.Invalid
    try:
        deviceID = DeviceID(deviceIDNumber)
    except:
        deviceID = DeviceID.Invalid
    return deviceID


def GetPacket(rawData, CRCPoly):
    packetType = GetPacketType(rawData[6])
    if (packetType == DataPacketType.Data_8bit_Small) or (packetType == DataPacketType.Data_8bit_Large):
        return  DataPacket(rawData, CRCPoly,8)

    elif (packetType == DataPacketType.Data_10bit_Small) or (packetType == DataPacketType.Data_10bit_Large):
        return DataPacket(rawData, CRCPoly,10)

    elif (packetType == DataPacketType.Impedance):
        return ImpedancePacket(rawData, CRCPoly)

    elif (packetType == DataPacketType.Thermal):
        return ThermalPacket(rawData, CRCPoly)

    elif (packetType == DataPacketType.BionodeConfig):
        return BionodeConfigPacket(rawData, CRCPoly)

    elif (packetType == DataPacketType.BaseStationConfig):
        return BaseStationConfigPacket(rawData, CRCPoly)

    elif (packetType == DataPacketType.IDResponse):
        return IDResponsePacket(rawData, CRCPoly)

    elif (packetType == DataPacketType.IDPing):
        return IDPingPacket(rawData, CRCPoly)

    else:
        return Packet(rawData, CRCPoly)

class BaseStationController():
    def __init__(self, BytesPerBSPacket, CRCPoly, UDP_TX_Port, UDP_RX_Port, UDP_TX_IP, UDP_RX_IP, CommFrequency=0):
        self.BytesPerBSPacket = BytesPerBSPacket
        # Initialize to use short packets
        self.BytesPerBNodePacket = SMALL_PACKET_LENGTH
        self.CRCPoly = CRCPoly
        self.UDP_TX_Port = UDP_TX_Port
        self.UDP_RX_Port = UDP_RX_Port
        self.UDP_RX_IP = UDP_RX_IP
        self.UDP_TX_IP = UDP_TX_IP
        if (self.UDP_TX_IP == '127.0.0.1'):
            port = 9000
        else:
            port = 0

        #Setup RX and TX sockets
        self.socket_TXRX = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #socket.inet_aton(self.UDP_RX_IP)
        self.socket_TXRX.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
        #self.socket_TXRX.bind((self.UDP_RX_IP, self.UDP_RX_Port))
        self.socket_TXRX.bind(('',port))
        myIP, myPort = self.socket_TXRX.getsockname()
        print("Bound to", myIP+":"+str(myPort))
        self.PID = 0
        self.CommFrequency = CommFrequency
        self.ResetTimestampTimer = False
        self.CoilDriveOn = False

    def CloseSockets(self):
        #self.socket_TXRX.shutdown(socket.SHUT_RDWR)
        #self.socket_TXRX.shutdown(socket.SHUT_RDWR)

        self.socket_TXRX.close()
        self.socket_TXRX.close()

    def UpdateBionodeRegisters(self, bionodeVersion, amplitude, pulseRepeatTime, pulseWidth, numStimCycles, stimOnTime, stimOffTime, totalSampleRate, ADCResolution, StartStim, StopStim, StartImpedanceTest1, StopImpedanceTest1, StartImpedanceTest2, StopImpedanceTest2, StartThermalTest, ADCChannelRatio, PositiveCalibration, NegativeCalibration,CathodicCalibration,AnodicCalibration, ZeroCalibration, ADC1InputCode, ADC2InputCode, ADC3InputCode, ADC4InputCode, StimWaveformRegister, InterphasicDelay, BatterySwitchConfig):
        #First, define hardcoded registers
        handshakeInterval = 0x64
        ADC_InputRegister = 0x0000

        if (totalSampleRate <= 25000):
            ADC_Prescale = 0x07
            # Will use small packet size
            self.BytesPerBNodePacket = SMALL_PACKET_LENGTH
        else:
            ADC_Prescale = 0x02
            # will use large packet size
            self.BytesPerBNodePacket = LARGE_PACKET_LENGTH

        if (float(bionodeVersion) < 4.1):
            ADC_InputRegister |= (ADC1InputCode << 3)
            ADC_InputRegister |= ADC2InputCode
        else:
            ADC_InputRegister |= (ADC4InputCode << 12)
            ADC_InputRegister |= (ADC3InputCode << 8)
            ADC_InputRegister |= (ADC2InputCode << 4)
            ADC_InputRegister |= ADC1InputCode

        #ADC_InputRegister = 0x0A #Top = CH1, Bottom = CH2

        #Set stim config (0 = nothing, 1 = Start Stim, 2 = Stop Stim)
        if (StartStim):
            stimTimerConfig = 0x01
        elif (StopStim):
            stimTimerConfig = 0x02
        else:
            stimTimerConfig = 0x00

        if(StartImpedanceTest1):
            impedanceTest1Config = 0x01
        elif (StopImpedanceTest1):
            impedanceTest1Config = 0x02
        else:
            impedanceTest1Config = 0x00

        if (StartImpedanceTest2):
            impedanceTest2Config = 0x01
        elif (StopImpedanceTest2):
            impedanceTest2Config = 0x02
        else:
            impedanceTest2Config = 0x00

        if (StartThermalTest):
            thermalTestConfig = 0x01
        else:
            thermalTestConfig = 0x00

        if (ADCChannelRatio == '1:1'):
            ADCChannelSwitchConfig = 0x00
        elif (ADCChannelRatio == '1:3'):
            ADCChannelSwitchConfig = 0x01
        else:
            ADCChannelSwitchConfig = 0x02

        #First, translate user settings to appropriate register settings
        if (float(bionodeVersion) >= 4.0):
            amplitudeSetting = int(4096/1.8*(800*amplitude+0.9))
        else:
            amplitudeSetting = int(4096 * 200 * amplitude/1.2)

        ADC_CC0Setting = int(16000000/math.pow(2,ADC_Prescale)/totalSampleRate)
        pulseRepeatTimeSetting = int(pulseRepeatTime*1000000)
        pulseWidthSetting = int(pulseWidth*1000000)
        interphasicDelaySetting = int(InterphasicDelay*1000000)

        #Now, construct a Bionode Config Packet
        packet = []

        #Sync bytes
        packet.append(0xA5)                                     # -2
        packet.append(0x5A)                                     # -1

        #PID bytes
        packet.append(0xFF & (self.PID>>8))                     # 0
        packet.append(self.PID&0xFF)                            # 1

        #TX ID Byte
        packet.append(DeviceID.PC.value&0xFF)                   # 2

        #RX ID Byte
        packet.append(DeviceID.Bionode.value&0xFF)              # 3

        #Packet Type bytes
        packet.append(DataPacketType.BionodeConfig.value&0xFF)  # 4

        #Target ID. SHOULD BE REMOVED!!
        packet.append(0x00&0xFF)                                # 5
        #Comm Frequency. SHOULD BE REMOVED!!
        packet.append(0x00&0xFF)                                # 6

        #Handshake interval. CONSIDER REMOVING!
        packet.append((handshakeInterval>>8)&0xFF)              # 7
        packet.append(handshakeInterval&0xFF)                   # 8

        #Amplitude
        packet.append((amplitudeSetting>>8)&0xFF)               # 9
        packet.append(amplitudeSetting&0xFF)                    # 10

        #Pulse Repeat Time
        packet.append((pulseRepeatTimeSetting&0xFF000000)>>24)  # 11
        packet.append((pulseRepeatTimeSetting&0x00FF0000)>>16)  # 12
        packet.append((pulseRepeatTimeSetting&0x0000FF00)>>8)   # 13
        packet.append((pulseRepeatTimeSetting&0x000000FF))      # 14

        #Pulse Width
        packet.append((pulseWidthSetting&0xFF000000)>>24)       # 15
        packet.append((pulseWidthSetting&0x00FF0000)>>16)       # 16
        packet.append((pulseWidthSetting&0x0000FF00)>>8)        # 17
        packet.append((pulseWidthSetting&0x000000FF))           # 18


        #Number of Stim Cycle
        packet.append((numStimCycles>>8)&0xFF)                  # 19
        packet.append(numStimCycles&0xFF)                       # 20

        #ADC Timer Prescale. Consider hardcoding
        packet.append(ADC_Prescale&0xFF)                        # 21

        #ADC Timer CC0 Register (total sample rate)
        packet.append(ADC_CC0Setting&0xFF)                      # 22

        if (float(bionodeVersion) < 4.1):
            packet.append(ADC_InputRegister&0xFF)               # 23
        else:
            packet.append((ADC_InputRegister>>8)&0xFF)          # 23
            packet.append(ADC_InputRegister&0xFF)               # 24

        #ADCResolution
        packet.append(ADCResolution&0xFF)                       # 25

        #Stim timer config setting
        packet.append(stimTimerConfig&0xFF)                     # 26

        #Impedance Test 1 Config
        packet.append(impedanceTest1Config&0xFF)                # 27

        #Impedance Test 2 Config
        packet.append(impedanceTest2Config&0xFF)                # 28

        #Thermal Test Config
        packet.append(thermalTestConfig&0xFF)                   # 29

        #ADC Channel Switch Config
        packet.append(ADCChannelSwitchConfig&0xFF)              # 30

        #PositiveStimCalibration
        packet.append(PositiveCalibration&0xFF)                 # 31
        #NegativeStimCalibration
        packet.append(NegativeCalibration&0xFF)                 # 32

        #PositiveStimCalibration
        packet.append(CathodicCalibration&0xFF)                 # 33
        #NegativeStimCalibration
        packet.append(AnodicCalibration&0xFF)                   # 34

        #Zero Calibration
        packet.append(ZeroCalibration&0xFF)                     # 35

        #StimWaveform register
        packet.append(StimWaveformRegister&0xFF)                # 36

        #Interphasic Delay
        packet.append((interphasicDelaySetting&0xFF000000)>>24) # 37
        packet.append((interphasicDelaySetting&0x00FF0000)>>16) # 38
        packet.append((interphasicDelaySetting&0x0000FF00)>>8)  # 39
        packet.append((interphasicDelaySetting&0x000000FF))     # 40

        print(stimOnTime,stimOffTime)

        packet.append(0xff & stimOnTime>>24)                    # 41
        packet.append(0xff & stimOnTime>>16)                    # 42
        packet.append(0xff & stimOnTime>>8)                     # 43
        packet.append(0xff & stimOnTime)                        # 44
        packet.append(0xff & stimOffTime>>24)                   # 45
        packet.append(0xff & stimOffTime>>16)                   # 46
        packet.append(0xff & stimOffTime>>8)                    # 47
        packet.append(0xff & stimOffTime)                       # 48

        #BatterySwitch State
        packet.append(BatterySwitchConfig&0xFF)                 # 49

        packetLength = len(packet)
        #Fill the rest of the payload with zeros
        packet.extend([0x00 for i in range(self.BytesPerBSPacket-packetLength-2)])

        #CRC (removing the SYNC bytes)
        crc = CRCGen(packet[2:len(packet)], CRCPoly)
        packet.append(crc&0x00FF)
        packet.append(crc>>8)

        #Check packet validity
        valid = CRCGen(packet[2:],CRCPoly) == 0

        #If not valid, don't send anything.
        if not valid:
            return False
        else:
            print(packet)
            #self.socket_TXRX.sendto(bytes(packet), (self.UDP_IP, self.UDP_TX_Port))
            self.socket_TXRX.sendto(bytes(packet), (self.UDP_TX_IP, self.UDP_TX_Port))

            #Update stored settings for implant!!

            self.PID = (self.PID + 1) &0xFFFF
            return True

    def SendShutDownPacketToBaseStation(self):
        packet = []

        #Sync bytes
        packet.append(0xA5)
        packet.append(0x5A)

        #PID bytes
        packet.append(self.PID>>8)
        packet.append(self.PID&0xFF)

        #TX ID Byte
        packet.append(DeviceID.PC.value&0xFF)

        #RX ID Byte
        packet.append(DeviceID.BaseStation.value&0xFF)

        #Packet Type bytes
        packet.append(DataPacketType.ShutDown.value&0xFF)
        packetLength = len(packet)

        #Fill the rest of the payload with zeros
        packet.extend([0x00 for i in range(self.BytesPerBSPacket-packetLength-2)])

        #CRC (removing the SYNC bytes)
        crc = CRCGen(packet[2:len(packet)], CRCPoly)
        packet.append(crc&0x00FF)
        packet.append(crc>>8)

        #Check packet validity
        valid = CRCGen(packet[2:],CRCPoly) == 0

        #If not valid, don't send anything.
        if not valid:
            return False
        else:
            print(packet)
            self.socket_TXRX.sendto(bytes(packet), (self.UDP_TX_IP, self.UDP_TX_Port))
            self.PID = (self.PID + 1) &0xFFFF
            return True

    def UpdateBSRegisters(self, CommFrequency, PAPowerRegister, ChargeFrequency, AutoChargeFrequency, PAEnable, TimeStampReset):

        
        ChargeFrequencySetting = int((float(ChargeFrequency)/16-16)*4096) & 0xFFFF


        packet = []

        #Sync bytes
        packet.append(0xA5)
        packet.append(0x5A)

        #PID bytes
        packet.append(self.PID>>8)
        packet.append(self.PID&0xFF)

        #TX ID Byte
        packet.append(DeviceID.PC.value&0xFF)

        #RX ID Byte
        packet.append(DeviceID.BaseStation.value&0xFF)

        #Packet Type bytes
        packet.append(DataPacketType.BaseStationConfig.value&0xFF)

        #BaseStation ID. This is read only, so fill with zero
        packet.append(0x00)

        #Comm Frequency
        packet.append(CommFrequency&0xFF)

        #PA Power
        packet.append(PAPowerRegister&0xFF)

        #Charge Frequency
        packet.append(ChargeFrequencySetting>>8)
        packet.append(ChargeFrequencySetting&0xFF)

        #Auto-frequency Power Enable. 
        if ( AutoChargeFrequency):
            packet.append(0x01)
        else:
            packet.append(0x00)

        #PA Enable
        if (PAEnable):
            packet.append(0x01)
        else:
            packet.append(0x00)

        #Fill the rest of the payload with zeros
        packetLength = len(packet)
        packet.extend([0x00 for i in range(self.BytesPerBSPacket-packetLength-2)])

        #TimeStamp Reset
        if (TimeStampReset):
            packet[50] = 0x01
        else:
            packet[50] = 0x00

        packetLength = len(packet)
        #Fill the rest of the payload with zeros
        packet.extend([0x00 for i in range(self.BytesPerBSPacket-packetLength-2)])

        #CRC (removing the SYNC bytes)
        crc = CRCGen(packet[2:len(packet)], CRCPoly)
        packet.append(crc&0x00FF)
        packet.append(crc>>8)

        #Check packet validity
        valid = CRCGen(packet[2:],CRCPoly) == 0

        #If not valid, don't send anything.
        if not valid:
            return False
        else:
            print(packet)
            #self.socket_TXRX.sendto(bytes(packet), (self.UDP_IP, self.UDP_TX_Port))
            self.socket_TXRX.sendto(bytes(packet), (self.UDP_TX_IP, self.UDP_TX_Port))
            self.PID = (self.PID + 1) &0xFFFF
            return True